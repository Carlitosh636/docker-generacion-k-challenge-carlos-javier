Use docker-compose --d --build to create the containers. 
--build is required as each component of the project has its own Dockerfile. Docker compose uses those files to build the containers,
not online images.
