import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { Dog } from './dogs/dog';
import { DogsModule } from './dogs/dogs.module';

@Module({
  imports: [TypeOrmModule.forRoot({
    type: 'mysql',
    host: process.env.APP_DB_HOST,
    port: +process.env.APP_DB_PORT,
    username: process.env.APP_DB_USERNAME,
    password: process.env.APP_DB_PASSWORD,
    database: process.env.APP_DB_NAME,
    entities: [Dog],
    synchronize: true
  }), DogsModule],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
